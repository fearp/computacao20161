\documentclass{beamer}
\usepackage{xcolor}
\usepackage{hyperref}
\usepackage{ctable}
\usepackage{natbib}
\usepackage[utf8x]{inputenc}
\usepackage[brazilian]{babel}
%\usepackage{pgfplots}
%\usepackage{pgfplotstable}
\usepackage{amssymb, amsmath}
%\usepackage{tikz}
\usepackage{verbatim}
%\usepgfmodule{shapes}

%\usepackage{fancyvrb}
\usepackage{listings}

\usepackage{textcomp}
%\usepackage{ifthen}
%\usetikzlibrary{arrows,shapes}
\usepackage{color}

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}


\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\small,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,                    % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=R,                 % the language of the code
  otherkeywords={*,...},           % if you want to add more keywords to the set
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=true,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,                       % sets default tabsize to 2 spaces
  title=\lstname,                   % show the filename of files included with \lstinputlisting; also try caption instead of title
literate=
  {á}{{\'a}}1 {é}{{\'e}}1 {í}{{\'i}}1 {ó}{{\'o}}1 {ú}{{\'u}}1
  {Á}{{\'A}}1 {É}{{\'E}}1 {Í}{{\'I}}1 {Ó}{{\'O}}1 {Ú}{{\'U}}1
  {à}{{\`a}}1 {è}{{\`e}}1 {ì}{{\`i}}1 {ò}{{\`o}}1 {ù}{{\`u}}1
  {À}{{\`A}}1 {È}{{\'E}}1 {Ì}{{\`I}}1 {Ò}{{\`O}}1 {Ù}{{\`U}}1
  {ä}{{\"a}}1 {ë}{{\"e}}1 {ï}{{\"i}}1 {ö}{{\"o}}1 {ü}{{\"u}}1
  {Ä}{{\"A}}1 {Ë}{{\"E}}1 {Ï}{{\"I}}1 {Ö}{{\"O}}1 {Ü}{{\"U}}1
  {â}{{\^a}}1 {ê}{{\^e}}1 {î}{{\^i}}1 {ô}{{\^o}}1 {û}{{\^u}}1
  {Â}{{\^A}}1 {Ê}{{\^E}}1 {Î}{{\^I}}1 {Ô}{{\^O}}1 {Û}{{\^U}}1
  {œ}{{\oe}}1 {Œ}{{\OE}}1 {æ}{{\ae}}1 {Æ}{{\AE}}1 {ß}{{\ss}}1
  {ű}{{\H{u}}}1 {Ű}{{\H{U}}}1 {ő}{{\H{o}}}1 {Ő}{{\H{O}}}1
  {ç}{{\c c}}1 {Ç}{{\c C}}1 {ø}{{\o}}1 {å}{{\r a}}1 {Å}{{\r A}}1
  {€}{{\EUR}}1 {£}{{\pounds}}1
}

%\DefineVerbatimEnvironment{Sinput}{Verbatim}{xleftmargin=2em,frame=single}
%\DefineVerbatimEnvironment{Soutput}{Verbatim}{xleftmargin=2em,frame=single}
% \usepackage[usenames,dvipsnames,table]{xcolor}
% \pgfplotsset{compat=1.7}

\mode<presentation>
  {
	\usetheme{FEARP}
% 	\usetheme{Pittsburgh}
	% \setbeamercovered{transparent = 28}
	}

%\SweaveOpts{keep.source=TRUE}

\begin{document}
\SweaveOpts{concordance=TRUE}


\title{Introdução ao R - Aula 1} 
%\author{André Luiz Martins Pignata}
%\author{Alexandre Nicolella} 

\date{\today} 

\frame{\titlepage}


\frame{
	\frametitle{Introdução}
	O que é?
	\begin{itemize}
		\item<1-> O R é uma linguagem de programação voltada à manipulação de dados
		\item<2-> Possui ferramentas para gerenciamento de dados e cálculos estatísticos
		\item<3-> Integração com outras ferramentas como o Sweave (\LaTeX) e o SQL
		\item<4-> Possui vários pacotes para exibição de gráficos
	\end{itemize}
}

\frame{
    \frametitle{Onde encontro / Instalação}
    \textbf{R}
    \begin{itemize}
        \item O R é disponibilizado no site da CRAN: \hyperlink{https://cran.r-project.org/}{http://cran.r-project.org}
        
        \item Esse site também é o repositório dos pacotes oficiais que utilizaremos (o que são pacotes)
    \end{itemize}

    \textbf{RStudio}
    \begin{itemize}
        \item O RStudio é uma interface para R, que facilita e organiza nosso trabalho
        \item O RStudio está disponibilizado em \hyperlink{https://www.rstudio.com/}{https://www.rstudio.com/}
    \end{itemize}
}

\frame{
    \frametitle{RStudio}
    \includegraphics[width=.9\textheight]{images/rstudio.png}
}

\frame{
    \frametitle{RStudio}
    Utilização do RStudio
    \begin{itemize}
        \item Criação de novo arquivo .R
        \item Configuração do diretório de trabalho (working dir)
        \item Interface do RStudio
        \begin{itemize}
            \item Saída do console
            \item Aba environment
            \item Aba History
            \item Aba Plots
            \item Aba Packages (instalação do pacote gdata)
        \end{itemize}
        \item Utilização do HELP
    \end{itemize}
}

\frame{
    \frametitle{Boas práticas e definições}
    \begin{itemize}
        \item Como em toda linguagem de programação, o R trabalha com o conceito de variáveis para armazenamento de valores em memória
        \item Para definirmos uma variável em R basta criarmos um nome e atribuírmos um valor
        \item Algumas regras devem ser observadas para que criemos um código legível e de fácil manutenção, tanto para nós como para nossos companheiros de pesquisa
    \end{itemize}
    \lstinputlisting[language=R, firstline=1, lastline=1]{exemplos.R}


}
\frame{
    \frametitle{Boas práticas e definições}
    \begin{itemize}
        \item O nome das variáveis são CASE SENSITIVE
        \lstinputlisting[language=R, firstline=2, lastline=2]{exemplos.R}
        \item SEMPRE nomeie as variáveis de forma que o nome reflita sua utilização, mas tome cuidado com abreviações ou nomes muito extensos.
        \lstinputlisting[language=R, firstline=3, lastline=7]{exemplos.R}
    \end{itemize}
}

\begin{frame}[c]\frametitle{Boas práticas e definições}
    \begin{itemize}
        \item Uma boa prática é indicar o tipo da variável em seu nome (numérico, string, vetor, factor, dataframe)
        \lstinputlisting[language=R, firstline=9, lastline=13]{exemplos.R}
        
    \end{itemize}

\end{frame}

\begin{frame}[c]\frametitle{Boas práticas e definições}
    \begin{itemize}
        \item A indentação (ou identação) do código é uma das formas mais fáceis de se organizar e tornar o código legível. Consistem em adicionar espaços (usualmente 3) ou tabulações em blocos de código para indicar início e fim do mesmo.
        \lstinputlisting[language=R, firstline=16, lastline=31,basicstyle=\tiny]{exemplos.R}
    \end{itemize}

\end{frame}



\begin{frame}[fragile]{Tipos básicos de dados}
        Acessando posições nos vetores
<< echo=TRUE, include=TRUE,results=tex>>=
vGrupos <- c(10,20,30)
vGrupos[1:3]
vGrupos[1]
vGrupos[3:1]
@

\end{frame}

\begin{frame}[fragile]{Tipos básicos de dados}
        Strings
<< echo=TRUE, include=TRUE,results=tex>>=
a <- "olá mundo"
a
b <- c("olá","mundo")
b
b[1]
typeof(a)
typeof(b)
typeof(b[1])
@
\end{frame}

\begin{frame}[fragile]{Tipos básicos de dados}
Factors
\begin{itemize}
  \item Podemos entender o tipo de dado factor como um vetor com diversos níveis, ou seja, uma matriz multidimensional
  \item Os agrupamentos de um factor são os levels
<< echo=TRUE, include=TRUE,results=tex,results=verbatim>>=
nSatisfacao <- c(1,1,1,1,1,1,1,1,2,2,2,2,2,2 
                ,2,2,2,2,2,2,2,2,2,2,2,2,2,2
                ,2,2,2,3,3,3,3,3,3,3,3,3,3,4
                ,4,4,4,4,4,4,4,4,4,4,4,4)
summary(nSatisfacao)
nSatisfacao <- factor(nSatisfacao)
summary(nSatisfacao)
levels(nSatisfacao)
@
\end{itemize}

\end{frame}

\begin{frame}[fragile]{Tipos básicos de dados}
Data frame
\begin{itemize}
  \item Outro modo de armazenar os dados, são os data frames
  \item Os data frames serão os objetos que utilizaremos para manipular nossos dados,
  \item Podemos carregar os dados via arquivos (Excel, arquivos csv, arquivos com tamanho de coluna fixa, arquivos texto puro, etc..)
  \item No momento só citaremos, os exemplos e a forma de utilizar, serão exibidas mais a frente
\end{itemize}

\end{frame}

\begin{frame}[fragile]{Tipos básicos de dados}
Tipo lógico
\begin{itemize}
  \item No R trabalhamos com duas variáveis lógicas pré-definidas \emph{TRUE} e \emph{FALSE}
<< echo=TRUE, include=TRUE,results=tex>>=
a = TRUE
typeof(a)
b = !a
b
@
  \item Podemos utilizar operadores como 
  $ < \quad >\quad  <=\quad  >= \quad ==\quad  !=\quad  ||\quad  \&\&\quad  !\quad $
  
\end{itemize}

\end{frame}

\begin{frame}[fragile]{Tipos básicos de dados}
Matriz
  \begin{itemize}
    \item Outra forma de organizar os dados é em forma de matriz
    \item Esse tipo de dado é muito utilizado para regressões
<< echo=TRUE, include=TRUE,results=verbatim>>=
mDiagonal <- matrix(c(1,0,0,0,1,0,0,0,1),
                    ncol=3,byrow=TRUE)
mDiagonal
@
  \end{itemize}
\end{frame}
\iffalse
\frame{
    \frametitle{Exercícios Wooldridge}
    Nesse mini-curso utilizaremos algumas bases de dados disponíveis em
    \hyperlink{www.cengage.com/aise/economics/wooldridge\_3e\_datasets/}{
    www.cengage.com/aise/economics/wooldridge\_3e\_datasets/}
    \begin{itemize}
        \item Baixem o arquivo excelfiles.zip 
        \item Salvem no diretório de trabalho e descompactem
    \end{itemize}
}
\frame{
    \frametitle{Trabalhando com os dados}
    Dentro da pasta excelfiles existem dois tipos de arquivo:
    \begin{itemize}
        \item arquivos com extensão .xls \textrightarrow Dados
        \item arquivos com extensão .des \textrightarrow Descrição
    \end{itemize}
    \lstinputlisting[firstline=7, lastline=13,frame=single]{excelfiles/WAGE2.DES}

    % \lstinputlisting[language=R, firstline=1, lastline=19]{3_2.R}
}

\frame{
    \frametitle{Leitura dos dados}
    \lstinputlisting[language=R, firstline=1, lastline=10]{3_2.R}
}

\begin{frame}[fragile]{Data frame}
    \begin{itemize}
      \item Com o comando read.xls, criamos um Data Frame chamado dWage2
      \item Podemos visualizar os dados fazendo view(dWage2)
      \item Vamos utilizar esse Data Frame para aprendermos alguns outros comandos e operações
<< echo=false, include=false,results=hide>>=
library('gdata')
dWage2 = read.xls('excelfiles/wage2.xls',sheet=1,header=FALSE)
names(dWage2)[1] <- "wage"
names(dWage2)[2] <- "hours"
names(dWage2)[3] <- "IQ"
names(dWage2)[4] <- "KWW"
names(dWage2)[5] <- "educ"
names(dWage2)[6] <- "exper"
names(dWage2)[7] <- "tenure"
names(dWage2)[8] <- "age"
names(dWage2)[9] <- "married"
names(dWage2)[10] <- "black"
names(dWage2)[11] <- "south"
names(dWage2)[12] <- "urban"
names(dWage2)[13] <- "sibs"
names(dWage2)[14] <- "brthord"
names(dWage2)[15] <- "meduc"
names(dWage2)[16] <- "feduc"
names(dWage2)[17] <- "lwage"
@

<< echo=TRUE, include=TRUE,results=verbatim>>=
dWage2 = dWage2[c("educ","sibs","meduc","feduc")]
summary(dWage2)
@
    \end{itemize}
    
\end{frame}



\begin{frame}[fragile]{Limpeza dos dados}
  \begin{itemize}[<+->]
      \item<1-> No mundo real, são raros os casos em que as bases venham formatadas conforme precisamos
      \item<2-> Além de abrir as bases em um formato legível para nosso software (Data frame no nosso caso)
      \item<3-> Precisamos fazer uma \emph{Análise exploratória dos dados} para tentarmos verificar erros e inconsistências nos dados
      \item<4-> Técnicas de limpeza e análise fogem do escopo desse mini-curso, mas de uma maneira bem simplificada podemos verificar que o summary do nosso Data Frame retornou resultados não esperados (coluna \emph{meduc} e \emph{feduc} como factors ao invés de vetor numérico)
      \item<5-> \textbf{Por que isso ocorre?}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Limpeza dos dados}
  \begin{itemize}
    \item No nosso caso, vamos eliminar as variáveis com valor '.'
<< echo=TRUE, include=TRUE,results=verbatim>>=
dWage2 <- dWage2[dWage2$meduc!='.',]
dWage2 <- dWage2[dWage2$feduc!='.',]
@
    \item Também devemos converter nosso factor para numeric
<< echo=TRUE, include=TRUE,results=verbatim>>=
dWage2$meduc <- as.numeric(levels(dWage2$meduc))[
  dWage2$meduc]
dWage2$feduc <- as.numeric(levels(dWage2$feduc))[
  dWage2$feduc]
@
  \item Vamos ver o resultado
<< echo=TRUE, include=TRUE,results=verbatim>>=
summary(dWage2)
@
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Plotando gráficos simples}
  \begin{itemize}
    \item Uma das grandes facilitades que temos com o R é a criação de gráficos de forma simples e rápida
    \item Por exemplo
<< echo=TRUE, include=TRUE,results=tex,fig=TRUE,width=6,height=4.5>>=
plot(dWage2)
@
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Plotando gráficos simples}
<< echo=TRUE, include=TRUE,results=tex,fig=TRUE,width=10,height=8>>=
plot(dWage2$sibs)
@
\end{frame}

\begin{frame}[fragile]{Plotando gráficos simples}
<< echo=TRUE, include=TRUE,results=tex,fig=TRUE,width=10,height=8>>=
hist(dWage2$sibs)
@
\end{frame}

\begin{frame}[fragile]{Plotando gráficos simples}
<< echo=TRUE, include=TRUE,results=tex,fig=TRUE,width=10,height=8>>=
hist(dWage2$educ)
@
\end{frame}

\begin{frame}[fragile]{Plotando gráficos com ggplot}
<< echo=TRUE, include=TRUE,results=tex,width=10,height=8>>=
library('ggplot2')
library('ggthemes')
dTmp <- aggregate(meduc ~ sibs, dWage2,mean)
g <- ggplot(dTmp,aes(x=sibs,y=meduc))+
    ggtitle('Educação da mãe')+
    geom_line(colour="blue", linetype="dotted", size=1.5)
@
\end{frame}

\begin{frame}[fragile]{Plotando gráficos com ggplot}
<< echo=TRUE, include=TRUE,results=tex,fig=TRUE,width=10,height=8>>=
g
@
\end{frame}


\begin{frame}[fragile]{Plotando gráficos com ggplot}
<< echo=TRUE, include=TRUE,results=tex,width=10,height=8>>=
library('ggplot2')
library('ggthemes')
dTmp <- aggregate(sibs ~ meduc, dWage2,mean)
g <- ggplot(dTmp,aes(x=meduc,y=sibs,fill=meduc))+
    ggtitle('Educação da mãe')+
    theme_economist()+scale_colour_economist()+
    geom_bar(stat="identity")
@
\end{frame}

\begin{frame}[fragile]{Plotando gráficos com ggplot}
<< echo=TRUE, include=TRUE,results=tex,fig=TRUE,width=10,height=8>>=
g
@
\end{frame}

\begin{frame}[fragile]{Plotando gráficos com ggplot}
<< echo=TRUE, include=TRUE,results=tex,width=10,height=8>>=
library('ggplot2')
library('ggthemes')
dTmp <- aggregate(meduc ~ sibs, dWage2,mean)
g <- ggplot(dTmp,aes(x=sibs,y=meduc))+
    ggtitle('Educação da mãe')+
    theme_economist()+scale_colour_economist()+
    geom_line(colour="blue", linetype="dotted", size=1.5) 
@
\end{frame}

\begin{frame}[fragile]{Plotando gráficos com ggplot}
<< echo=TRUE, include=TRUE,results=tex,fig=TRUE,width=10,height=8>>=
g
@
\end{frame}

\begin{frame}[fragile]{Plotando gráficos com ggplot}
<< echo=TRUE, include=TRUE,results=tex,fig=TRUE,width=10,height=8>>=
g+geom_line(colour="red", linetype="dashed", size=.5) 
@
\end{frame}

\begin{frame}[fragile]{Plotando gráficos com ggplot}
<< echo=TRUE, include=TRUE,results=tex,fig=TRUE,width=10,height=8>>=
g+geom_line(colour="black", size=1) 
@
\end{frame}

\begin{frame}[fragile]{Plotando gráficos com ggplot}
<< echo=TRUE, include=TRUE,results=verbatim,fig=False,width=10,height=8>>=
dTmpfeduc <- aggregate(feduc ~ sibs, dWage2,mean)
g<-g+geom_line(colour="black",data=dTmpfeduc
            ,aes(x=sibs,y=feduc), size=1)+
  ggtitle('Educação da mãe e do pai')+
  scale_fill_discrete(name="Educação",
                         breaks=c("feduc", "meduc"),
                         labels=c("Pai", "Mãe"))
  
@
\end{frame}

\begin{frame}[fragile]{Plotando gráficos com ggplot}
<< echo=TRUE, include=TRUE,results=tex,fig=TRUE,width=10,height=8>>=
g
@
\end{frame}

\begin{frame}[fragile]{Comandos para econometria}
Obtendo informações sobre os dados do objeto
\tiny{
<< echo=TRUE, include=TRUE,results=verbatim,fig=False,width=10,height=8>>=
summary(dWage2)
@
}
\normalsize 
\phantom{ xxxx}
Transformação de variáveis de um data frame em matriz
\tiny
<< echo=TRUE, include=TRUE,results=verbatim,fig=False,width=10,height=8>>=
x <-as.matrix(cbind(int=1,dWage2$sibs,dWage2$meduc,dWage2$feduc))
x[1:4,]
@

\vdots\ldots
<< echo=TRUE, include=TRUE,results=verbatim,fig=False,width=10,height=8>>=
nrow(x)
ncol(x)
@

\end{frame}

\begin{frame}[fragile]{Comandos para econometria}
Criando um vetor, a partir de um data frame
\tiny{
<< echo=TRUE, include=TRUE,results=verbatim,fig=False,width=10,height=8>>=
y <- as.vector(dWage2$educ)
y[1:20]
length(y)
@
}
\normalsize 
\phantom{ xxxx}
Criando uma matriz diagonal
\tiny{
<< echo=TRUE, include=TRUE,results=verbatim,fig=False,width=10,height=8>>=
i <- diag(1,nrow=3,ncol=3)
i
@
}

\end{frame}

\begin{frame}[fragile]{Comandos para econometria}
Operações com matrizes
\phantom{xxx}


Fazendo a transposta de uma matriz
\tiny{
<< echo=TRUE, include=TRUE,results=verbatim,fig=False,width=10,height=8>>=
x <-as.matrix(cbind(int=1,c(1,2,3,4),c(5:8),c(8:11)))
x
t(x)
@



\end{frame}

\begin{frame}[fragile]{Comandos para econometria}
Operações com matrizes
\phantom{xxx}
Multiplicação matricial \%*\%
<< echo=TRUE, include=TRUE,results=verbatim,fig=False,width=10,height=8>>=
x%*%t(x)
@

Resolvendo uma equação matricial no format a * x = b
\tiny{
<< echo=TRUE, include=TRUE,results=verbatim,fig=False,width=10,height=8>>=
x[1,4] = 100
x <- x[1:3,2:4]
solve(x)
@



\end{frame}


\begin{frame}[fragile]{Comandos para econometria}
Operações com vetores / variáveis / data frames
\phantom{xxx}

Somatório de um vetor
\tiny
<< echo=TRUE, include=TRUE,results=verbatim,fig=False,width=10,height=8>>=
y <- c(1:10)
y
sum(y)
@
\normalsize
Valor médio de um vetor
\tiny
<< echo=TRUE, include=TRUE,results=verbatim,fig=False,width=10,height=8>>=
mean(y)
@

\normalsize
Variância 
\tiny
<< echo=TRUE, include=TRUE,results=verbatim,fig=False,width=10,height=8>>=
var(dWage2$educ)
var(dWage2$meduc)
var(dWage2$feduc)
@
\end{frame}

\begin{frame}[fragile]{Comandos para econometria}
Operações com vetores / variáveis / data frames
\phantom{xxx}

\normalsize
Co-variância 
\tiny
<< echo=TRUE, include=TRUE,results=verbatim,fig=False,width=10,height=8>>=
cov(dWage2$meduc,dWage2$feduc)
@

\normalsize
Correlação 
\tiny
<< echo=TRUE, include=TRUE,results=verbatim,fig=False,width=10,height=8>>=
nVarMeduc <- var(dWage2$meduc)
nVarFeduc <- var(dWage2$feduc)
nCov <- cov(dWage2$meduc,dWage2$feduc)
nCov/sqrt(nVarMeduc*nVarFeduc)
cor(dWage2$meduc,dWage2$feduc)

@

\end{frame}

\begin{frame}[fragile]{Comandos para econometria}
Regressão linear simples

\phantom{xxx}
\normalsize
Usando a função lm
\tiny
<< echo=TRUE, include=TRUE,results=verbatim,fig=False,width=10,height=8>>=
fit <- lm(dWage2$educ ~ dWage2$feduc)
summary(fit)
@

\phantom{xxx}
\normalsize
Obtendo os coeficientes
\tiny
<< echo=TRUE, include=TRUE,results=verbatim,fig=False,width=10,height=8>>=
coefficients(fit)
@
\phantom{xxx}
\normalsize


\end{frame}


\begin{frame}[fragile]{Comandos para econometria}
Regressão linear simples

Obtendo os resíduos
\tiny
<< echo=TRUE, include=TRUE,results=verbatim,fig=False,width=10,height=8>>=
residuals(fit)
@


\end{frame}

\begin{frame}[fragile]{Comandos para econometria}
Regressão linear simples com mais de uma variável explicativa

\phantom{xxx}
\normalsize
Usando a função lm
\tiny
<< echo=TRUE, include=TRUE,results=verbatim,fig=False,width=10,height=8>>=
fit <- lm(dWage2$educ ~ dWage2$feduc + dWage2$meduc + dWage2$sibs)
summary(fit)
@

\end{frame}
\fi
\end{document}
